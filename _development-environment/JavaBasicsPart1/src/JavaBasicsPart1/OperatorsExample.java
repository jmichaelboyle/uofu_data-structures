/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JavaBasicsPart1;

/**
 *
 * @author Nirmal
 */
public class OperatorsExample {
    public static void main(String[] args) {
        int a = 10;
        int b = 5;
        int c,d,e,r;
        int u1;
        
        //Arithmetic Operators
        c = a+b;
        System.out.println("The value of c: "+c);
        
        
        d = c-8;
        System.out.println("The value of d: "+d);
         
        
        e = 5*10;
        System.out.println("The value of e: "+e);
        
        
        double f = 14.0/4.0; 
        int i = (int)(14.0/4.0); // Example of casting - double value rounded off to fit  in integer datatype
        r = 14%4;
        System.out.println("The value of f: "+f);
        System.out.println("The value of i: "+i);
        System.out.println("The value of r: "+r);
        
        //Unary operators
        u1=10;
        
        int u2 = u1++;
        System.out.println("The value of u2: "+u2);
        System.out.println("The value of u1 after computing u2: "+u1);
        
        int u3 = ++u1;
        System.out.println("The value of u3: "+u3);
        System.out.println("The value of u1 after computing u3: "+u1); //Try for -- operator and see the values
        
        // logical complement operator
        boolean positive = true;
        boolean complementOperation = !(positive); 
        System.out.println("The value of complementOperation: "+complementOperation);
        
        //Operator precedence
        d = 4*a+b;
        e = b+a*4; // Both d and e variables should have same value
        System.out.println("The value of d :"+d);
        System.out.println("The value of e :"+e); 
    }
}
