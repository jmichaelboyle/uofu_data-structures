/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exam2preparationexamples4415;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author mboyle
 */
public class Exam2PreparationExamples4415 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ////////////////////////
        // OO Concepts
        ////////////////////////
        SimpleCalculator mySimpleCalculator1 = new SimpleCalculator();
        mySimpleCalculator1.operand1 = 3.1f;
        mySimpleCalculator1.operand2 = 4.2f;
        System.out.println(mySimpleCalculator1.add());
        System.out.println(mySimpleCalculator1.subtract());
        System.out.println(mySimpleCalculator1.multiply());
        System.out.println(mySimpleCalculator1.divide());

        //this is a new instance of SimpleCalculator with different state
        SimpleCalculator mySimpleCalculator2 = new SimpleCalculator();
        mySimpleCalculator2.operand1 = 30.1f;
        mySimpleCalculator2.operand2 = 400.2f;
        System.out.println(mySimpleCalculator1.add());

        //TODO(student): create another instance can call subtract() on it
        ////////////////////////
        // IO
        ////////////////////////
        //TODO(student): create the code to write the following String variables to a 
        // a file called output_file.txt
        String myRecord1 = "CHI,20,SAAD,Wrist,Off. Zone,53";
        String myRecord2 = "CHI,27,ODUYA,Wrist,Off. Zone,48";

        //TODO(student): create the code to read from file and print to the console only
        // the second record (i.e. "CHI,27,ODUYA,Wrist,Off. Zone,48")
        ////////////////////////
        // Exceptions
        ////////////////////////
        try {
            //TODO(student): print the text "in the try block 1"
            BufferedReader myBufferedReader = new BufferedReader(
                    new FileReader("some-file-that-does-not-exist.txt"));
            //TODO(student): print the text "in the try block 2"
        } catch (IOException ex) {
            //TODO(student): print ex.getMessage()
            //TODO(student): print the text "in the catch block"
        } finally {
            //TODO(student): print the text "in the finally block"
            //TODO(student): add code to close the file
        }

        try {
            //TODO(student): print the text "in the try block 1"
            BufferedReader myBufferedReader = new BufferedReader(
                    new FileReader("a-file-that-exists.txt"));
            //TODO(student): print the text "in the try block 2"
        } catch (IOException ex) {
            //TODO(student): print ex.getMessage()
            //TODO(student): print the text "in the catch block"
        } finally {
            //TODO(student): print the text "in the finally block"
            //TODO(student): add code to close the file
        }

        /////////////////////
        //methods
        /////////////////////
        //TODO(student): create a method called add that takes two integers
        // and returns the addition
        //TODO(student): print the result of the method without assigning it
        // to a variable first
                //TODO(student): execute and experiment with the code below
        //TODO(student): review all lecture materal from Lecture 13: OOP part 1 
        // through Lecture 21: Colections 
        //TODO(student): execute and experiment with lecture-related code in 
        //alignment with lecture material
        //TODO(student): do assignment 5 (even though it can be submitted the
        // the week after the exam
        //TODO(student): review other assignment solutions
        //TODO(student): execute and experiment with the code below (again)
        ////////////////
        //Data encapsultion
        ////////////////
        //TODO(student): redo the exercise in the lecture code
        // "OOPBicycleExampleModifiers" in order to understand data encapsulation
        /////////////////
        //Modifiers
        /////////////////
        //TODO(student): redo the exercise in the lecture code "OOPModifiers"
        // in order to understand the different modifiers, private, protected
        // and public
        ////////////////
        //Variables and Classes
        ////////////////
        //TODO(student): create a new class called ClassForVariableExperimentation
        //TODO(student): add an instance variable and experiment with it from this class
        //TODO(student): add an final variable and experiment with it from this class
        //TODO(student): add an static variable and experiment with it from this class
        //TODO(student): add an static final variable and experiment with it from this class
        ///////////////////
        // Object references
        //////////////////
        SomeClass myClass1 = new SomeClass(34);
        //TODO(student): create a new variable that points to the instance above
        //TODO(student): create a new variable that points to a separate 
        // instance above that also has the state set to 34
        //TODO(student): verify that there are only two objects, but three variables
        // by using the debugger

        //////////////////////
        // Static methods
        /////////////////////
        //TODO(student): create a new class called ClassForMethodExperimentation
        //TODO(student): add a static method that returns an int and experiment 
        // with it from this class; note that an instance need not be created 
        // to call this method
        //TODO(student): add a static method that returns nothing and experiment 
        // with it from this class; note that an instance need not be created 
        // to call this method
        //TODO(student): add an instance method that returns an int and experiment 
        // with it from this class; note that an instance need not be created 
        // to call this method
        //TODO(student): add an instance method that returns nothing and experiment 
        // with it from this class; note that an instance need not be created 
        // to call this method
        ////////////////////
        // Constructors
        ////////////////////
        //TODO(student): create a Bicycle class with a constructor
        //TODO(student): create a MountainBike class which inherits from Bicycle
        // and implements two different constructors
        //TODO(student): experiment with all three constructors with this class
        ///////////////////
        // Overriding vs. overloading
        ///////////////////
        //TODO(student): review the lecture code 
        // "ObjectRelationshipsOverridingAnimalCat" along with lecture 
        // material to understand overriding
        //TODO(student): on this class create two methods called doSomething 
        // where one is defined as public static void doSomething(int someIntParam)
        // and the other is defined as doSomething(float myFloatParam).  What is
        // this called?
        ///////////////////
        // Interfaces and Inheritance
        //////////////////
        //TODO(student): create two interfaces each with one method
        // a) USBInterface, b) BasicPhoneInterface and a class MobileDevice 
        // that implements both interfaces
        /////////////////////
        // Polymorphism
        /////////////////////
        //TODO(student): create a base class called Bird and two subclasses called
        // Duck and Sparrow
        // TODO(student): create a single array of type Bird and place both Duck
        // and Robin instances in this array
        /////////////////////
        // Collections and sorting
        /////////////////////
        //TODO(student): create a Student class that implements the Comparable interface
        //TODO(student): create an array of students and sort them according to 
        // the natural sort order defined when implementing the Comparable interface
        //TODO(student): create an ArrayList of students and sort them according to 
        // the natural sort order defined when implementing the Comparable interface
        //TODO(student): create a Comparator to override the natural sorting behavior
        // on the student class; similar code can be found in the lecture code
        // "CollectionsExample"
        //TODO(student): implement Bubble Sort using an array of Students as opposed
        // to an array of integers
        //////////////////////
        // ADTs
        //////////////////////
        //TODO(student): study that ADTs covered in the lectures; given a business
        // requirement choose between a List, Set, Queue, Map, Stack or Deque.
        //TODO(student): redo the examples in the lecture code, "AbstractDataTypes"
        // and make sure you are familiar with the interfaces (e.g. List) vs. 
        // the concrete implementations (e.g. ArrayList).
    }
}
