/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javabasicspart2;

import java.util.Arrays;

/**
 *
 * @author Nirmal
 */
public class ArrayCompare {
    public static void main(String args[]){
    char[] a = {'a','b','c','d'};
    char[] b = {'a','b','c','d'};
    char[] c = {'a','b','c','e'};
    char[] d = c;
       
    
    System.out.println("Arrays a and b are "+(Arrays.equals(a, b)?"":"not ")+"equal"); //Using Arrays class equals method
    System.out.println("Using equals() directly on array variable - a and b are "+(a.equals(b)?"":"not ")+"equal");
            
    System.out.println("Arrays a and c are "+(Arrays.equals(a, c)?"":"not ")+"equal");
    System.out.println("Using equals() directly on array variable - b and c are "+(d.equals(c)?"":"not ")+"equal");
    }
    
}
