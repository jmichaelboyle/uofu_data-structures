/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objectrelationshipsoverridinganimalcat;

/**
 *
 * @author mboyle
 */
public class ObjectRelationshipsOverridingAnimalCat {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Animal.testClassMethod();

//        Animal.testClassMethod();
//        Animal myAnimal = new Animal();
//        myAnimal.testInstanceMethod();
        Cat myCat = new Cat();
        Animal myAnimal = myCat;
        myAnimal.testInstanceMethod();

        Cat myCat2 = new Cat();
        myCat2.testInstanceMethod();
        
        
    }
    
}
