/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mainpackage;

/**
 *
 * @author mboyle
 */
public class SomeBaseClass {
    public int myPublicInt = 8;
    protected int myProtectedInt = 9;
    int myPackageInt = 10;
    private int myPrivateInt = 11;
    
    public void setMyPrivateInt(int value){
        myPrivateInt = value;
    }
    
    public int getPrivateInt(){
        //TODO(student): what is interesting about accessing this variable?
        System.out.println(doSomethingPrivate());
        return myPrivateInt;
    }
    private int doSomethingPrivate(){
        return 99;
    }
    
}
