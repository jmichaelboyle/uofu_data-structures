/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JavaBasicsPart1;

/**
 *
 * @author Nirmal
 */
public class ControlFlowExamples {
    
 public static void main(String[] args) {   
    int a = 10;
    int b = 5;
    
    // IF - ELSE example
    if (a>=b){
        System.out.println("a is greater than b");
    }
    else {
        System.out.println("b is greater than a");
    }
    //Run the above program by changing the value of a and b
    
    // IF - ELSE IF example
    if (a>b){
        System.out.println("a is greater than b");
    }
    else if (a == b) {
        System.out.println("a is equal to b");
    }
    else {
        System.out.println("b is greater than a");
    }
    
    // USING ?: OPERATOR
    
    int c = (a>b)?a:b;
    System.out.println("The greatest among "+a+" and "+b+" is "+c); // check what happens if a and b are equal
    
 }  
    
}
