/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objectrelationshipscomposition;

import java.util.ArrayList;

/**
 *
 * @author mboyle
 */
public class Course {
    
    private final String courseName;
    private final ArrayList<Student> studentList = new ArrayList<>(); 
    
    public Course(String courseName){
        this.courseName = courseName;
    }
    
    public String getCourseName(){
        return courseName;
    }
    
    public int getCourseCurrentSize(){
        return studentList.size();
    }
    
    public boolean addStudent(Student newStudent){
        boolean addResult = studentList.add(newStudent);
        return addResult;
    }
    public void printStudentsInCourse(){
        for(int i = 0; i < studentList.size(); i++){
            Student currStudent = studentList.get(i);
            System.out.println(currStudent.getStudentName());
        }
    }

    
}
