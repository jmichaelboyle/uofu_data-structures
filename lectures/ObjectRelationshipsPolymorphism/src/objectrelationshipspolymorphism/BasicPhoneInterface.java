/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objectrelationshipspolymorphism;

/**
 *
 * @author mboyle
 */
public interface BasicPhoneInterface {
    public void dial(int number);
    public void dial(int areaCode, int number);
    public void hangup();
}
