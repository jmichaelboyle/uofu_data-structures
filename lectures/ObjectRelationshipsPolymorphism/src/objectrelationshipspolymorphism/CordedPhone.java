/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objectrelationshipspolymorphism;

/**
 *
 * @author mboyle
 */
public class CordedPhone implements BasicPhoneInterface {
     public void dial(int number){
        System.out.println("dialing with a number and a cord");
    }
    public void dial(int areaCode, int number){
        System.out.println("dialing with area and number  and a cord");
    }
    public void hangup(){
        System.out.println("hanging up with cord");
    }
}
