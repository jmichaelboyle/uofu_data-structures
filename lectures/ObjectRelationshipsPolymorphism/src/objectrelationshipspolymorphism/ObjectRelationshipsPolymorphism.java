/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objectrelationshipspolymorphism;

/**
 *
 * @author mboyle
 */
public class ObjectRelationshipsPolymorphism {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Bicycle[] myBikeArr = new Bicycle[2];
        Bicycle myMountainBike = new MountainBike();
        myBikeArr[0] = myMountainBike;
        
        Bicycle myRoadBike = new RoadBike();
        myBikeArr[1] = myRoadBike;
        
        for(int i = 0; i < myBikeArr.length; i++){
            myBikeArr[i].printBikeDetails();
        }
        
        MobilePhone myMobile = new MobilePhone();
        BasicPhoneInterface myCordedPhone = new CordedPhone();
        BasicPhoneInterface[] myPhoneArray = new BasicPhoneInterface[2];
        myPhoneArray[0] = myMobile;
        myPhoneArray[1] = myCordedPhone;
        
        for(int i = 0; i < myPhoneArray.length; i++){
            myPhoneArray[i].hangup();
        }
        
    }
    
}
