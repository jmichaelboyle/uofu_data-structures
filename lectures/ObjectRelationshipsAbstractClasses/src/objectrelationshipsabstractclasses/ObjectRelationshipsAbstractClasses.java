/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objectrelationshipsabstractclasses;

/**
 *
 * @author mboyle
 */
public class ObjectRelationshipsAbstractClasses {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       Circle myCircle = new Circle();
       myCircle.draw();
       myCircle.resize();
       myCircle.moveTo(4, 6);
       
       //TODO(student): attempt to create an instance of Shape
       
       //TODO(student): create a rectangle that is a Shape
       Rectangle myRectangle = new Rectangle();
       //TODO(student): create a shape array and put both a Circle and a
       //rectangle in it
       
       Shape[] shapeArr = new Shape[2];
       shapeArr[0] = myCircle;
       shapeArr[1] = myRectangle;
       
       //TODO(student): examine each array item, calling instanceof on each
       
       for(int i = 0; i < shapeArr.length; i++){
           Shape currShape = shapeArr[i];
           currShape.moveTo(6, 7); //using a common method to all shapes
           if (currShape instanceof Circle){
               System.out.println("it's a circle");
               Circle currCircle = (Circle)currShape;
               currCircle.calculateCircumference();
           } else if (currShape instanceof Rectangle){
               System.out.println("it's a rectangle");
               Rectangle currRectangle = (Rectangle)currShape;
               currRectangle.calculateDiagonalLength();
           }
       }
       
    }
    
}
