/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package overflowandbreak;

/**
 *
 * @author mboyle
 */
public class OverflowAndBreak {
    public static void main(String[] args) {
        int total = 0;
        for (int square = 1, grains = 1; square <= 64; square++) {
            grains *= 2;
            System.out.println("the value of grains is: " + grains);
            if (grains <= 0) {
                System.out.println("Error: overflow on variable grains");
                break;
            }
            total += grains;
            System.out.println(total);
        }
        System.out.println("All done!");
    }
}
