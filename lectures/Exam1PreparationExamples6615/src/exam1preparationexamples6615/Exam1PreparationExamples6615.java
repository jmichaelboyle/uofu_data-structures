/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exam1preparationexamples6615;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author mboyle
 */
public class Exam1PreparationExamples6615 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //TODO(student): execute and experiment with the code below (in-class)
        //TODO(student): review all lecture materal from Lecture 1: getting started with 
        // java through Lecture 12: IO Errors and Exceptions
        //TODO(student): execute and experiment with lecture-related code in alignment 
        // with lecture material
        //TODO(student): from scratch, redo A00
        //TODO(student): review solutions for A1 and A2
        //TODO(student): execute and experiment with the code below (after reviewing 
        // material)
        
        ///////////////////
        // comments
        //////////////////
        //this is a single-line comment
        /*
         * this is a block comment
         */
        
        ///////////////////////
        //variable naming conventions
        ///////////////////////
        int goodIntVariableName = 0;
        int BADINTVARIABLENAME = 0; //TODO(student): Why is this bad?
        int BadIntVariableName = 0; //TODO(student): Why is this bad?
        int bad_int_variable_name = 0; //TODO(student): Why is this bad?
        
        ///////////////////////
        //redeclaring a variable == compile error
        ///////////////////////
        int myInt1 = 0;
        //Note: Can't redeclare the variable: int myInt1 = 0;
        myInt1 = 99; //can reassign a variable
        
        ///////////////////
        // primitives - integrals
        ///////////////////
        byte myByte = -60;
        //can't do this: byte myByte2 = 227;
        short myShort2 = 32767;
        //can't do this: short myShort3 = 42767;
        //TODO(student): What are the min and max values for:
        //byte, short, int, float, double
        
        ///////////////////////
        //compilation errors 
        ///////////////////////
        //TODO(student): Which ones don't compile and why?
        double myDouble = 1;
        float myFloat = 1.2f;
        //int myInt2 = null;
        //boolean myBool = ;
        //char myChar = "hello";
        //String myString = 'a';
        //TODO(student): What other cases should we try?
        
        ///////////////////
        // escape sequences
        ///////////////////
        String myString21 = "hello\n";
        System.out.println(myString21);
        String myString31 = "that\'s it!";
        System.out.println(myString31);
        String myString41 = "the path is, c:\\temp";
        System.out.println(myString41);
        String myString51 = "col1\tcol2\tcol3\n";
        System.out.println(myString51);
        //TODO(student): print out: this is in "quotes"
        System.out.println("quotes");
        
        //TODO(student): print out: this is in 'single-quotes'
        
        ///////////////////////
        //casting
        ///////////////////////
        int myInt3 = 23;
        double myDouble3 = myInt3;
        //myInt3 = myDouble3; //TODO(student): Why doesn't this work?
        myDouble3 = 23.7;
        myInt3 = (int)myDouble3;
        
        int myInt4 = 0;
        double myDouble2 = 3.4;
        String myString2 = new String("3.4");
        boolean myBool2 = true;
        //myDouble2 = myString2; //TODO(student): How would you get this cast to work?
        //boolean myBool3 = myDouble2.equals(myString2);
        myDouble2 = (double)myInt4++;
        myDouble2 = myDouble2 + 3.4;
        //myDouble2 += (double)myBool2; //TODO(student): Why doesn't this work?

        ///////////////////////
        //prefix and postfix operators
        ///////////////////////
        //TODO(student): Observe and understand the behavior of prefix 
        // and postfix operators.
        int myInt5 = 10;
        System.out.println(myInt5);
        System.out.println(myInt5++);
        System.out.println(myInt5);
        System.out.println(++myInt5);
        System.out.println(myInt5);
        
        //TODO(student): Perform the same logic as above without using the 
        //prefix and postfix operators
        
        ///////////////////////
        // for loops
        ///////////////////////
        //Note different ways to define parts of the for loop.
        for(int i = 2; i < 34; i = i + 12){
            System.out.println("i is: " + i);
        }
        
        for(int i = 100; i > 50; i = i - 30){
            System.out.println("i is: " + i);
        }
        
        //TODO(student): create a new for loop with a complex definition
   
        /////////////////////////
        // blocks
        /////////////////////////
        //Note how blocks define the scope for a variable.
        int myInt7 = 10; 
        {
            //int myInt7 = 10; //this is not okay as the upper block is avialable 
            int myInt8 = 10;
        }
        int myInt8 = 10; // this is okay because it is not in the same block
        
        //TODO(student): add another block within the block above and
        // redeclare myInt8
        
        /////////////////////////
        // arrays
        /////////////////////////
        //There are two ways to declare an array.
        int[] myIntArray1;
        int myIntArray2[];
        
        float[] myFloatArray1 = new float[5];
        for(int i = 0; i < myFloatArray1.length; i++){
            System.out.println(myFloatArray1[i]);
            //TODO(student): What are the default values of the array elements?
        }
        
        myFloatArray1[1+2] = 49.7f;
        for(int i = 0; i < myFloatArray1.length; i++){
            System.out.println(myFloatArray1[i]);
            //Note the value of the fourth element.
        }
        
        //TODO(student): do array comparison examples from arrays lecture code
        
        System.out.println("Note that this is a field (not a method): " +
                myFloatArray1.length);
        
        ////////////////////////
        // Wrapper classes
        ////////////////////////
        int myInt9 = Integer.parseInt("23");
        char myChar1 = Character.toLowerCase('C');
        //TODO(student):What methods exist on each of the Wrapper 
        // classes (including others)?  For each method that is interesting,
        // try it out.
        
        ////////////////////////
        // Strings
        ////////////////////////
        //different ways to declare and instantiate String objects
        String myString9 = "A String";
        String myString10 = new String("Another String");
        String myString11 = null;
        myString11 = "Yet another String";
     
        String myString12 = "A String"; // note that this points to the same location as myString9
        String myString13 = new String("A String"); // note that this points to a different location than myString9
        
        //TODO(student): do String comparison examples from Strings lecture code
        
        System.out.println("Note that this is a method call (not a field): " +
                myString13.length());
        
        //TODO(student): experiment with String class methods used in the 
        // assignments such as substring, indexOf, etc.
        
        ////////////////////////
        // OO Concepts
        ////////////////////////
        SimpleCalculator mySimpleCalculator1 = new SimpleCalculator();
        mySimpleCalculator1.operand1 = 3.1f;
        mySimpleCalculator1.operand2 = 4.2f;
        System.out.println(mySimpleCalculator1.add());
        System.out.println(mySimpleCalculator1.subtract());
        System.out.println(mySimpleCalculator1.multiply());
        System.out.println(mySimpleCalculator1.divide());
        
        //this is a new instance of SimpleCalculator with different state
        SimpleCalculator mySimpleCalculator2 = new SimpleCalculator();
        mySimpleCalculator2.operand1 = 30.1f;
        mySimpleCalculator2.operand2 = 400.2f;
        System.out.println(mySimpleCalculator1.add());
        
        //TODO(student): create another instance can call subtract() on it
        
        ////////////////////////
        // IO
        ////////////////////////
        //TODO(student): create the code to write the following String variables to a 
        // a file called output_file.txt
        String myRecord1 = "CHI,20,SAAD,Wrist,Off. Zone,53";
        String myRecord2 = "CHI,27,ODUYA,Wrist,Off. Zone,48";
        
        //TODO(student): create the code to read from file and print to the console only
        // the second record (i.e. "CHI,27,ODUYA,Wrist,Off. Zone,48")
        
        ////////////////////////
        // Exceptions
        ////////////////////////
        try{
            //TODO(student): print the text "in the try block 1"
            BufferedReader myBufferedReader = new BufferedReader(
                    new FileReader("some-file-that-does-not-exist.txt"));
            //TODO(student): print the text "in the try block 2"
        } catch (IOException ex) {
            //TODO(student): print ex.getMessage()
            //TODO(student): print the text "in the catch block"
        } finally {
            //TODO(student): print the text "in the finally block"
            //TODO(student): add code to close the file
        }
        
        try{
            //TODO(student): print the text "in the try block 1"
            BufferedReader myBufferedReader = new BufferedReader(
                    new FileReader("a-file-that-exists.txt"));
            //TODO(student): print the text "in the try block 2"
        } catch (IOException ex) {
            //TODO(student): print ex.getMessage()
            //TODO(student): print the text "in the catch block"
        } finally {
            //TODO(student): print the text "in the finally block"
            //TODO(student): add code to close the file
        }
        
        /////////////////////
        //methods
        /////////////////////
        //TODO(student): create a method called add that takes two integers
        // and returns the addition
        //TODO(student): print the result of the method without assigning it
        // to a variable first
        
    }
}
