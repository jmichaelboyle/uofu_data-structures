/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package iolecture;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author mboyle
 */
public class IOLecture {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {

        BufferedReader myBReader = null;
        BufferedWriter myBWriter = null;
        try {
            myBReader = new BufferedReader(
                    new FileReader("input.txt"));
            myBWriter = new BufferedWriter(new FileWriter("output.txt"));
            
            String curLine = myBReader.readLine();
            while (curLine != null) {
                System.out.println("current line is: " + curLine);
                myBWriter.write(curLine + "\n");
                curLine = myBReader.readLine();
            }
            
        } catch (FileNotFoundException fnfe) {
            System.out.println(fnfe.getMessage());
        } finally {
            System.out.println("in the finally block; closing BR and BW if applicable");
            if (myBReader != null) {
                myBReader.close();
            }
            if (myBWriter != null){
                myBWriter.close();
            }
        }
    }

}
