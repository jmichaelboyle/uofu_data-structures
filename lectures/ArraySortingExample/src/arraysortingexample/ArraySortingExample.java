/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arraysortingexample;

import java.util.Arrays;

/**
 *
 * @author mboyle
 */
public class ArraySortingExample {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String[] myStringArray = {"the", "quick", "brown", "fox"};
        System.out.println(Arrays.toString(myStringArray));
        Arrays.sort(myStringArray);
        System.out.println(Arrays.toString(myStringArray));
    }

}
