/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objectrelationshipscomposition;

/**
 *
 * @author mboyle
 */
public class Student {
    
    private final String studentName;
    
    public Student(String studentName){
        this.studentName = studentName;
    }
    
    public String getStudentName(){
        return this.studentName;
    }
    
}
