/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javabasicscontrolflowloopsbreakcontinueexample;

import java.util.Scanner;

/**
 *
 * @author mboyle
 */
public class JavaBasicsControlFlowLoopsBreakContinueExample {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner console = new Scanner(System.in);

        String userInput;
        do {
            System.out.println("enter any value to go to second-level loop:");
            userInput = console.nextLine();
            if (userInput.equals("exit current loop")) {
                break;
            }
            do {
                System.out.println("enter any value to go to third-level loop:");
                userInput = console.nextLine();
                if (userInput.equals("exit current loop")) {
                    break;
                }
                do {
                    System.out.println("enter any value to go to stay in the third-level loop:");
                    userInput = console.nextLine();
                    if (userInput.equals("exit current loop")) {
                        break;
                    }
                } while (!userInput.equals("exit current loop"));
            } while (!userInput.equals("exit current loop"));
        } while (!userInput.equals("exit current loop"));

    }

}
