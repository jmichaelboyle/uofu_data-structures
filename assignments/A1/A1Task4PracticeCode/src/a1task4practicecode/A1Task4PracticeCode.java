/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package a1task4practicecode;

/**
 *
 * @author mboyle
 */
public class A1Task4PracticeCode {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String eventTextNoHTML = "CHI won Neu. Zone - T.B #91 STAMKOS vs CHI "
                + "#26 HANDZUS";
        char[] eventTextNoHTMLCharArr = eventTextNoHTML.toCharArray();
        
        //this loop prints each character in the array one at a time
        for(int i = 0; i < eventTextNoHTMLCharArr.length; i++){
            char currentChar = eventTextNoHTMLCharArr[i];
            System.out.println(currentChar);
        }
        
        //this loop prints only the A's in the string
        for(int i = 0; i < eventTextNoHTMLCharArr.length; i++){
            char currentChar = eventTextNoHTMLCharArr[i];
            if(currentChar == 'A'){
                System.out.println(currentChar);
            }
        }
        
        //this loop counts the number of A's in the string which is printed
        //after the loop
        int counterOfLetterA = 0;
        for(int i = 0; i < eventTextNoHTMLCharArr.length; i++){
            char currentChar = eventTextNoHTMLCharArr[i];
            if(currentChar == 'A'){
                counterOfLetterA = counterOfLetterA + 1;
            }
        }
        System.out.println("The count of A's is: " + counterOfLetterA);
        
        //this loop counts the number of digits in the string which is printed
        //after the loop
        int counterOfDigits = 0;
        for(int i = 0; i < eventTextNoHTMLCharArr.length; i++){
            char currentChar = eventTextNoHTMLCharArr[i];
            if(Character.isDigit(currentChar)){
                counterOfDigits = counterOfDigits + 1;
            }
        }
        System.out.println("The count of digits is: " + counterOfDigits);
            
    }
    
}
