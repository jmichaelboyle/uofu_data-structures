/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package algorithms;

/**
 *
 * @author mboyle
 */
public class Algorithms {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int[] myArrayToSort1 = {5, 1, 4, 2, 8};
        printIntArray(myArrayToSort1);
        bubbleSort(myArrayToSort1);
        printIntArray(myArrayToSort1);

        int[] myArrayToSort2 = {5, 1, 4, 2, 8};
        printIntArray(myArrayToSort2);
        selectionSort(myArrayToSort2);
        printIntArray(myArrayToSort2);

    }

    public static void bubbleSort(int[] arrayToSort) {
        //TODO(student): implemement bubble sort
    }

    public static void selectionSort(int[] arrayToSort) {
        //TODO(student): implemement selection sort
    }

    public static void printIntArray(int[] arrayToPrint) {
        System.out.print("{");
        for (int i = 0; i < arrayToPrint.length; i++) {
            System.out.print(arrayToPrint[i] + ",");
        }
        System.out.print("\b}\n");
    }

}
