/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oopvariablesandclasses;

/**
 *
 * @author mboyle
 */
public class Bicycle {

    private int gear = 1; //instance variable; global to class
    private static int numBikesCreated = 0; //class variable; global to class

    public Bicycle(int gear){
        this.gear = gear;
        numBikesCreated += 1;
    }
    
    public static int getNumBikesCreated(){
        return numBikesCreated;
    }
    
    public int getGear(){
        return gear;
    }
    
    public void doSomethingBikeLike() {
        String myLocalString = "myLocalStringVariable";
    }

    public static void doSomethingBikeLike2(int anIntParameter, int someOtherParameter) {
        //TODO(student): why are we able to do this?
        String myLocalString = "myLocalStringVariable";
        
        System.out.println("the parameter value is: " + anIntParameter);
        //TODO(student): add another parameter and print its value
        System.out.println("the other parameter value is: " + someOtherParameter);
    }

}
