/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javabasicspart2;

/**
 *
 * @author Nirmal
 */
public class ArrayBasics {

    /**
     * @param args the command line arguments
     */
    @SuppressWarnings("empty-statement")
    public static void main(String[] args) {
        int[] integerArray;
        integerArray = new int[10];
        char charArray[] = {'a','b','c','d'};
        float[] floatArray = new float[5];
        
        
        //filling the array dynamically
        for(int i = 0;i<10; i++)
        {
            integerArray[i] = (i+1)*2;
        }
        
        // To get the length of the array
        int arrayLength = integerArray.length;
        
        // To read back from an array
        for(int i=0;i<arrayLength;i++)
        {
            System.out.println("The value of integerArray["+i+"]: "+integerArray[i]);
        }
    }
    
}
