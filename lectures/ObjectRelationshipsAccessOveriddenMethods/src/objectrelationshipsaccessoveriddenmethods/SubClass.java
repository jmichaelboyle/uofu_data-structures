/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objectrelationshipsaccessoveriddenmethods;

/**
 *
 * @author mboyle
 */
public class SubClass extends SuperClass {
    
    @Override
    public void printMethod(){
        super.printMethod();
        System.out.println("in subclass!");
        
    }
    public void yetAnotherMethod(){
        System.out.println("in yet another method in the subclass");
        someOtherMethod();
    }
    
//    public void someOtherMethod(){
//        
//    }
    
    
}
