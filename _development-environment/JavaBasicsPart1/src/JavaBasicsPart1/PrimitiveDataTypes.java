/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JavaBasicsPart1;

/**
 *
 * @author Nirmal
 */
public class PrimitiveDataTypes {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //Primitive Datatypes - integral
        char capitalC = 'C';
        byte byteVariable = 100;
        short shortVariable = 10000;
        int intVariable = 100000;
        
        //Primitive Datatypes - Floating Point
        float floatVariable = 12.25f;
        double doubleVariable = 125.25;
        
        
        //Primitive Datatypes - Booelan
        boolean positive = true; 
        boolean negative = false;
        
        //
        
        //Printing the variables
        System.out.print(capitalC);
        System.out.print('\t');             // Note the escape sequence and its effect on output
        System.out.print(byteVariable);
        System.out.print('\n');
        System.out.print(shortVariable);
        System.out.print('\n');
        System.out.print(intVariable);
        System.out.print('\n');
    }
    
}
