/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package collectionsexample;

/**
 *
 * @author mboyle
 */
public class Student implements Comparable {
   String uNID;
   String lastName;
   int age;
   float GPA;
   
   public Student(String uNID, String lastName
           , int age, float GPA){
       this.uNID = uNID;
       this.lastName = lastName;
       this.age = age;
       this.GPA = GPA;
   }

   @Override
   public String toString(){
       return lastName + " | " + age + " | " + GPA;
   }
   
//   @Override
//   public int compareTo(Object obj){
//       int returnVal = 0;
//       Student otherStudent = (Student)obj;
//       
//       if (this.GPA < otherStudent.GPA){
//           returnVal = -1;
//       } else {
//           returnVal = 1;
//       }
//       return returnVal;
//   }
//   
   
   @Override
   public int compareTo(Object obj1){
       Student otherStudent = (Student) obj1;
       int result = 0;
       if(this.age != otherStudent.age){
           result = this.age - otherStudent.age;
       } else if (!this.lastName.equals(otherStudent.lastName)){
           result = otherStudent.lastName.compareTo(this.lastName);
       } else {
           result = otherStudent.uNID.compareTo(this.uNID);
       }
       
       return result ;
   }
   
}
