/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objectrelationshipsabstractclasses;

/**
 *
 * @author mboyle
 */
public class Rectangle extends Shape{
        @Override
    public void resize(){
        System.out.println("resizing rectangle");
    }
    @Override
    public void draw(){
        System.out.println("drawing retangle");
    }
    
    //something that is rectangle specific
    public float calculateDiagonalLength(){
        System.out.println("calculating diagonal length");
        return 89.5f;
    }
    
}
