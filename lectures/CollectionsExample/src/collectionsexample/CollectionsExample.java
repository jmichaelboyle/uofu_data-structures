/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package collectionsexample;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author mboyle
 */
public class CollectionsExample {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Student myStudent1 = new Student("123", "Smith", 30, 3.4f);
        Student myStudent2 = new Student("456", "Allen", 30, 3.7f);
        Student myStudent3 = new Student("002", "Jones", 30, 4.0f);
        
 //       System.out.println(myStudent1.compareTo(myStudent2));
 //       System.out.println(myStudent2.compareTo(myStudent1));
        
        
        List<Student> myStudentList = new ArrayList<>();
        myStudentList.add(myStudent1);
        myStudentList.add(myStudent2);
        myStudentList.add(myStudent3);
        
        System.out.println(myStudentList);
        
        Collections.sort(myStudentList);
        
        System.out.println(myStudentList);
        
        Collections.sort(myStudentList, new StudentComparator());
        
        System.out.println(myStudentList);
    }
    
}
