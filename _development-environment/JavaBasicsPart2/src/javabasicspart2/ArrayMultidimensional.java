/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javabasicspart2;

/**
 *
 * @author Nirmal
 */
public class ArrayMultidimensional {
    public static void main(String args[])
    {
        int[][] intArray = {{1,2,3},{4,5,6}}; 
        String studentDetails[][] = {{"U0001","John","Doe"},{"U0002","Stuart","Little"}};
        
        int[][] dynamicArray = new int[2][3];
        
        
        for (int row=0;row<2;row++)
        {
            for(int col=0;col<3;col++)
            {
                dynamicArray[row][col] = intArray[row][col]*2;
                System.out.print(dynamicArray[row][col]+"\t");
            }
            System.out.println("");
        }
        
    }
}
