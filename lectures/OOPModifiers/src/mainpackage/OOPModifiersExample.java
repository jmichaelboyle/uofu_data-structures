/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mainpackage;
import somenewpackage.SomeOutOfPackageChildClass;

/**
 *
 * @author mboyle
 */
public class OOPModifiersExample {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        SomeBaseClass myBaseClass = new SomeBaseClass();
        System.out.println(myBaseClass.myPublicInt);
        System.out.println(myBaseClass.myProtectedInt);
        System.out.println(myBaseClass.myPackageInt);
        
        
        SomeInPackageChildClass myInPackageChildClass = 
                new SomeInPackageChildClass();
        System.out.println(myInPackageChildClass.myPublicInt);
        System.out.println(myInPackageChildClass.myProtectedInt);
        System.out.println(myInPackageChildClass.myPackageInt);
       
        
        SomeOutOfPackageChildClass myOutOfPackageChildClass = 
                new SomeOutOfPackageChildClass();
        System.out.println(myOutOfPackageChildClass.myPublicInt);
        System.out.println(myOutOfPackageChildClass.myProtectedInt);
        //TODO(Student): why doesn't the below statement work.
        //System.out.println(myOutOfPackageChildClass.myPackageInt);
        
        
    }
    
}
