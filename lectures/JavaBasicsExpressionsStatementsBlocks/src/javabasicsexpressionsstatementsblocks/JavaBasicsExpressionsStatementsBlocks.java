/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javabasicsexpressionsstatementsblocks;

/**
 *
 * @author mboyle
 */
public class JavaBasicsExpressionsStatementsBlocks {

    static int myIntGlobal = 45; //variable that is global in scope
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        int myIntInMainMethod = 49;
        
        //expressions and statements
        int myInt = 3 + 4; //simple expression
        int myInt2 = 3 * 2 + 4; //compound expression
        
        //blocks
        //int myInt = 10; //can't redeclare the same variable in the same block
        {
            //int myInt = 10; //this is not okay as the upper block is avialable 
            int myInt3 = 10;
        }
        int myInt3 = 10; // this is okay because it is not in the same block
        
        System.out.println("I can print myIntGlobal: " + myIntGlobal);
        System.out.println("I can also print myIntMainMethod: " 
                + myIntInMainMethod); //this variable is in the current scope
        
        someMethod(); //now call someMethod
    }
    public static void someMethod(){
        System.out.println("I can still print myIntGlobal: " + myIntGlobal);
        //System.out.println("I cannot print myIntInMainMethod: " 
        //+ myIntInMainMethod); //this variable is NOT in the current scope
    }
}
   