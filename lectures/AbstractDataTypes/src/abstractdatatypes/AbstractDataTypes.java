/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package abstractdatatypes;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Scanner;
import java.util.Set;
import java.util.Stack;
import java.util.TreeMap;

/**
 *
 * @author mboyle
 */
public class AbstractDataTypes {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        List<String> myList = new ArrayList<>();
        Set<String> mySet = new HashSet<>();
        Map<Integer, String> myMap = new TreeMap<>();
        Queue<String> myQueue = new LinkedList<>();
        Stack myStack = new Stack();
        
        Scanner console = new Scanner(System.in);
        System.out.println("Enter a word:");
        String userInput = console.nextLine();

        //TODO(student): add userInput to the List, Set, Map, Queue and Stack
        // two times
        myList.add(userInput);
        mySet.add(userInput);
        myMap.put(myMap.size() + 1, userInput);
        myQueue.add(userInput);
        myStack.push(userInput);
        
        System.out.println(myList);
        System.out.println(mySet);
        System.out.println(myMap);
        System.out.println(myQueue);
        System.out.println(myStack);
        
        myList.add(userInput);
        mySet.add(userInput);
        myMap.put(myMap.size() + 1, userInput);
        myQueue.add(userInput);
        myStack.push(userInput);
        
        System.out.println(myList);
        System.out.println(mySet);
        System.out.println(myMap);
        System.out.println(myQueue);
        System.out.println(myStack);
        
        //TODO(student): remove one of the items from the List, Set, Map, 
        // Queue and Stack
        myList.remove(0);
        mySet.remove("hello");
        myMap.remove(1);
        myQueue.remove();
        myStack.pop();
        
        System.out.println("list:" + myList);
        System.out.println("set:" + mySet);
        System.out.println("map:" + myMap);
        System.out.println("queue:" + myQueue);
        System.out.println("stack:" + myStack);
        
        //TODO(student): choose a different concrete implementation for the
        //Set and the Map
        
    }
    
}
