/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oopconstructors;

/**
 *
 * @author mboyle
 */
public class Player {

    private final String leagueName;
    private final String proPlayerID;
    private String lastName;
    private String firstName;
    private int birthYear;
    private int birthMonth;
    private int birthDay;
    private String country;
    private String city;

    //this constructor is used for professional players who already
    // have IDs assigned by the professional league
    public Player(String leagueName, String proPlayerID) {
        this.leagueName = leagueName;
        this.proPlayerID = proPlayerID;
    }

    //TODO(student): create a constructor for amateur players who  
    // do not have IDs assigned by professional league; it should include
    // all variables except leagueName and proPlayerID
    public Player(String lastName, String firstName,
            int birthYear, int birthMonth, int birthDay,
            String country, String city) {
        leagueName = "NA";
        proPlayerID = "NA";
        this.lastName = lastName;
        this.firstName = firstName;
        this.birthYear = birthYear;
        this.birthMonth = birthMonth;
        this.birthDay = birthDay;
        this.country = country;
        this.city = city;

    }

//    public Player(String messagetoPrint, String firstName,
//            int birthYear, int birthMonth, int birthDay,
//            String country, String city) {
//        leagueName = "NA";
//        proPlayerID = "NA";
//        this.firstName = firstName;
//        this.birthYear = birthYear;
//        this.birthMonth = birthMonth;
//        this.birthDay = birthDay;
//        this.country = country;
//        this.city = city;
//
//    }
    
    String getProPlayerDetails() {
        return this.leagueName + ", " + this.proPlayerID;
    }
    
    String getPlayerDetails(){
        return this.lastName + ", " + this.firstName;
        //TODO(MB): add later
    }
}
