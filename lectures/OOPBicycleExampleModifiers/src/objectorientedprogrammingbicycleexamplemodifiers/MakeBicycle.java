/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objectorientedprogrammingbicycleexamplemodifiers;

import javax.swing.text.AbstractDocument;

/**
 *
 * @author mboyle
 */
public class MakeBicycle {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Bicycle myBike1 = new Bicycle(1,2,3);
        Bicycle myBike2 = new Bicycle(4,5,6);
        System.out.println("the speed of myBike1 is: " + myBike1.getSpeed());
        System.out.println("the speed of myBike2 is: " + myBike2.getSpeed());
        System.out.println("the cadence of myBike1 is: " + myBike1.cadence);
        System.out.println("the cadence of myBike2 is: " + myBike2.cadence);
        
        
    }
    
}
