/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objectorientedconcepts;

/**
 *
 * @author mboyle
 */
public class Calculator {
    public float operand1;
    public float operand2;
    
    public float add(){
        return operand1 + operand2;
    }
    public float multiply(){
        return operand1 * operand2;
    }
    public float divide(){
        return operand1 / operand2;
    }
    public float subtract(){
        return operand1 - operand2;
    }
}
