/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javabasicscompositesarrays;

import java.util.Arrays;

/**
 *
 * @author mboyle
 */
public class JavaBasicsCompositesArrays {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        doExamplesFromSlides();
        //doArrayBasics();
        //doArrayCompare();
        //doMultiDimArray();
    }

    public static void doArrayBasics() {
        int[] integerArray;
        integerArray = new int[10];
        char charArray[] = {'a', 'b', 'c', 'd'};
        float[] floatArray = new float[5];

        //filling the array dynamically
        for (int i = 0; i < 10; i++) {
            integerArray[i] = (i + 1) * 2;
        }

        // To get the length of the array
        int arrayLength = integerArray.length;

        // To read back from an array
        for (int i = 0; i < arrayLength; i++) {
            System.out.println("The value of integerArray[" + i + "]: " + integerArray[i]);
        }
    }

    public static void doArrayCompare() {
        char[] a = {'a', 'b', 'c', 'd'};
        char[] b = {'a', 'b', 'c', 'd'};
        char[] c = {'a', 'b', 'c', 'e'};
        char[] d = c;
       
        System.out.println("Arrays a and b are " + (Arrays.equals(a, b) ?
                "" : "not ") + "equal"); //Using Arrays class equals method
        System.out.println("Using equals() directly on array variable - a and "
                + "b are " + (a.equals(b) ? "" : "not ") + "equal");

        System.out.println("Arrays a and c are " + (Arrays.equals(a, c) ? "" : "not ") + 
                "equal");
        System.out.println("Using equals() directly on array variable - d and c "
                + "are " + (d.equals(c) ? "" : "not ") + "equal");
    }

    public static void doMultiDimArray() {
        int[][] intArray = {{1, 2, 3}, {4, 5, 6}};
        String studentDetails[][] = {{"U0001", "John", "Doe"}, {"U0002", "Stuart", "Little"}};

        int[][] dynamicArray = new int[2][3];

        for (int row = 0; row < 2; row++) {
            for (int col = 0; col < 3; col++) {
                dynamicArray[row][col] = intArray[row][col] * 2;
                System.out.print(dynamicArray[row][col] + "\t");
            }
            System.out.println("");
        }
    }

    public static void doExamplesFromSlides() {
        int[] arrayOfInts = new int[3];
        arrayOfInts = new int[30];
        float[] arrayOfFloats = new float[7];
        String[] arrayOfStrings = new String[5];
        String[] arrayOfStrings2 = new String[5];
        arrayOfStrings2[0] = "Toyota";
        arrayOfStrings2[1] = "Honda";
        arrayOfStrings2[2] = "Audi";
        arrayOfStrings2[3] = "BMW";
        arrayOfStrings2[4] = "Ford";
        String[] arrayOfStrings3 = {"Toyota", "Honda", "Audi", "BMW", "Ford"}; //this is different
        String[] arrayOfStrings4 = null;

        System.out.println("arrayOfStrings2 first element: " + arrayOfStrings2[0]);

        float[] squares = new float[6];
        for (int i = 0; i < squares.length; i++) {
            squares[i] = i * i;
        }

        for (int i = 0; i < squares.length; i++) {
            System.out.println("Element: " + squares[i]);
        }

        int a[] = {1,2,3};
        int b[] = a;
       
        
        b[0] = 55;
        a[1] = 33;
        System.out.println("a[0] " + a[0] + " is the same as b[0]" + b[0]);

        int myInt1 = 42;
        int myInt2 = myInt1;
        System.out.println("myInt1 " + myInt1 + " is the same as myInt2" + myInt2);
        myInt1 = 49;
        System.out.println("myInt1 " + myInt1 + " is not the same as myInt2" + myInt2);
    }
}
