/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package objectorientedconcepts;

/**
 *
 * @author mboyle
 */
public class OtherCalc implements CalcInt{
   
    public int add(int op1, int op2){
        return op1 + op2;
    }
    
    public int subtract(int op1, int op2){
        return op1 - op2;
    }
    
}
