/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javabasicsstrings;

/**
 *
 * @author mboyle
 */
public class JavaBasicsStrings {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        doStringBasics();
        doStringInequality();
        doStringEqualityWithEqualsOperator();
        doStringEqualityWithEqualsMethod();
    }

    public static void doStringBasics() {
        String direct = "Hello World";
        String strVariable = new String();
        strVariable = "Welcome";

        String catVariable = direct + " " + strVariable;
        System.out.println(direct);
        System.out.println(strVariable + " to Java");
        System.out.println(catVariable);

        int stringLength = direct.length();
        String intToString = String.valueOf(stringLength);
        System.out.println(catVariable);
    }

    public static void doStringInequality() {
        String myString1 = "apple";
        String myString2 = "car";

//        //cannot user inequality operators on strings
//        if (myString1 > myString2){
//            System.out.println("myString1 is greater");
//        } else {
//            System.out.println("myString1 is not greater");
//        }
        //instead use the compareTo method on the String class
        if (myString1.compareTo(myString2) > 0) {
            System.out.println("myString1 is greater");
        } else {
            System.out.println("myString1 is not greater");
        }

    }

    public static void doStringEqualityWithEqualsOperator() {

        //example 1
        String str1 = new String("Hello World!");
        String str2 = new String("Hello World!"); //note that str2 is created with new keyword
        if (str1 == str2) {
            System.out.println("same");
        } else {
            System.out.println("not the same");
        }

        //example 2
        String str3 = "Hello World!";
        String str4 = "Hello World!";
        if (str3 == str4) {
            System.out.println("same");
        } else {
            System.out.println("not the same");
        }
    }

    public static void doStringEqualityWithEqualsMethod() {

        //example 1
        String str1 = new String("Hello World!");
        String str2 = new String("Hello World!"); //note that str2 is created with new keyword
        if (str1.equals(str2)) {
            System.out.println("same");
        } else {
            System.out.println("not the same");
        }

        //example 2
        String str3 = "Hello World!";
        String str4 = "Hello World!";
        if (str3.equals(str4)) {
            System.out.println("same");
        } else {
            System.out.println("not the same");
        }
    }

}
