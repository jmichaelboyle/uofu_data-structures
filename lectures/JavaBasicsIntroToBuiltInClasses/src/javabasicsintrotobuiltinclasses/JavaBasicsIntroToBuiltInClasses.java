/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javabasicsintrotobuiltinclasses;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

/**
 *
 * @author mboyle
 */
public class JavaBasicsIntroToBuiltInClasses {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        BigDecimal myTemperature = new BigDecimal(85.666667);
        BigDecimal myTemperatureRounded = myTemperature.setScale(
                2, RoundingMode.HALF_UP);
        System.out.println("myTemperature non-rounded: " + myTemperature);
        System.out.println("myTemperature non-rounded: " + myTemperatureRounded);
        
    }
    
}
