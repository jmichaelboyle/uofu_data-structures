/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JavaBasicsPart1;

/**
 *
 * @author Nirmal
 */
public class LoopExamples {
    public static void main(String[] args) {
        int count =0;
        
        // WHILE LOOP
        while(count < 10){
            System.out.println("In while loop, Count is "+count);
            count++;
        }
        // DO WHILE LOOP
        do{
            System.out.println("In do-while loop, Count is "+count);
            count++;
        }while(count<20); // Change the count value and condition value to 10 and check which loop executes atleast once
        
        // FOR LOOP
        
        for (int i = 1;i<10;i++){
            // code here
            System.out.println("In For loop, the value of i = "+i);
        }
        
    }
}
