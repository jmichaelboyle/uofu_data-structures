/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objectrelationshipsabstractclasses;

/**
 *
 * @author mboyle
 */
public abstract class Shape {
    int x = 0;
    int y = 0;
    
    public void moveTo(int newX, int newY){
        x = newX;
        y = newY;
        System.out.println("moving to new location");
    }
    
    public abstract void draw();
    public abstract void resize();
}
