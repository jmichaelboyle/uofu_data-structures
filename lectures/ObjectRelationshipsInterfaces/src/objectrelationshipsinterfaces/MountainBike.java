/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objectrelationshipsinterfaces;

/**
 *
 * @author mboyle
 */
public class MountainBike implements BicycleInterface{
    
    public int cadence;
    
    @Override
    public void applyBrakes(int decrement){
        System.out.println("applying Brakes");
    }
    
    @Override
    public void changeCadence(int newValue){
        cadence = newValue;
    }
    
    @Override
    public void changeGear(int newValue){
        System.out.println("changing gear");
    }
    @Override
    public void speedUp(int increment){
        System.out.println("speeding up");
    }
}
