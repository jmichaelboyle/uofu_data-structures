/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objectrelationshipsaccessoveriddenmethods;

/**
 *
 * @author mboyle
 */
public class SuperClass {
    public void printMethod(){
        System.out.println("In superclass!");
    }
    public final void someOtherMethod(){
        System.out.println("In some other method in the superclass");
    }
    
}
