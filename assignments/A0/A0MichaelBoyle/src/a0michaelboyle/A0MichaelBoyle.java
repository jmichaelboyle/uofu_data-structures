/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package a0michaelboyle;

/**
 *
 * @author mboyle
 */
public class A0MichaelBoyle {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //simple solution
        System.out.println("FINAL     1ST   2ND   3RD    OT      SO        T");
        System.out.println("---------------------------------------------------");
        System.out.println("SJS\t|  1  |  0  |  1  |  0  |  (2 - 2)  |  3  |");
        System.out.println("CHI\t|  0  |  0  |  2  |  0  |  (0 - 2)  |  2  |");
        System.out.println("---------------------------------------------------");
        
        //advanced solution
        printHeader();
        printSeparator();
        printData("SJS", 1, 0, 1, 0, 2, 2, 3);
        printData("CHI", 0, 0, 2, 0, 0, 2, 2);
        printSeparator();
    }
    
    public static void printData(String teamCode, int first, int second
            , int third, int ot, int soPart1, int soPart2, int total){
        System.out.println(teamCode + "\t|  " 
                + first + "  |  " 
                + second + "  |  "
                + third + "  |  " 
                + ot + "  |  "
                + "(" + soPart1 +" - "+ soPart2 + ")  |  " 
                + total + "  |");
    }
    
    public static void printHeader(){
        System.out.println("FINAL     1ST   2ND   3RD    OT      SO        T");
    }
    
    public static void printSeparator(){
        System.out.println("---------------------------------------------------");
    }
    
    
}
