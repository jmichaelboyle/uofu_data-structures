/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package JavaBasicsPart1;

/**
 *
 * @author Nirmal
 */
public class BranchingExample {
    public static void main(String[] args) {
        int count = 0;
        for(count=0;count<20;count++)
        {
            if(count>5 && count <= 10)     // AND relational operator
            {
                System.out.println("Inside IF flow,Count = "+count);
                continue;
            }
            else if(count >10)
            {
                System.out.println("Inside ELSE IF flow,Count i= "+count);
                break;
            }
                                               
            System.out.println("Inside For loop, Count is "+count);
            
        }
    }
}
