/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercisearraylist;

/**
 *
 * @author mboyle
 */
public class Player {
    private String fullName;
    
    Player(String fullName){
        this.fullName = fullName;
    }
    
    public String getFullName(){
        return fullName;
    }
}
