/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objectrelationshipsmountainbikeinherit;

/**
 * reference: http://docs.oracle.com/javase/tutorial/java/javaOO/classes.html
 * @author mboyle
 */
public class Bicycle {
    // the Bicycle class has
    // three fields
    protected int cadence;
    protected int gear;
    protected int speed;
        
    // the Bicycle class has
    // four methods
    public void setCadence(int newValue) {
        cadence = newValue;
    }
    
    public int getCadence() {
        return cadence;
    } 
   
    
    public void setGear(int newValue) {
        gear = newValue;
    }
        
    public void applyBrake(int decrement) {
        speed -= decrement;
    }
        
    public void speedUp(int increment) {
        speed += increment;
    }
}
