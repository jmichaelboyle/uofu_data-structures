/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objectrelationshipsabstractclasses;

/**
 *
 * @author mboyle
 */
public class Circle extends Shape {
    @Override
    public void resize(){
        System.out.println("resizing circle");
    }
    @Override
    public void draw(){
        System.out.println("drawing circle");
    }
    
    public float calculateCircumference(){
        System.out.println("calculating circumference");
        return 45.4f;
    }
}
