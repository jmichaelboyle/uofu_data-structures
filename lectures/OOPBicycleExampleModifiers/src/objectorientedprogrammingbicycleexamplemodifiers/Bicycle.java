/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objectorientedprogrammingbicycleexamplemodifiers;

/**
 *
 * @author mboyle
 */
public class Bicycle {

    //TODO(student): make cadence private and create the corresponding
    // getter and setter method
    public int cadence;
    public int gear;
    private int speed;

    public Bicycle(int startCadence, int startSpeed, int startGear) {
        gear = startGear;
        cadence = startCadence;
        speed = startSpeed;
    }


    public void setGear(int newValue) {
        gear = newValue;
    }

    public void applyBrake(int decrement) {
        speed -= decrement;
    }

    public void speedUp(int increment) {
        speed += increment;
    }
    
    public int getSpeed(){
        return speed;
    }
    
    
}
