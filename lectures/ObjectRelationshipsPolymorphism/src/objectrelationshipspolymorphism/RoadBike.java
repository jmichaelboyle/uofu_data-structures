/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objectrelationshipspolymorphism;

/**
 *
 * @author mboyle
 */
public class RoadBike extends Bicycle{
    public void printBikeDetails(){
        System.out.println("this isn't any ordinary bike...it's a roadbike");
    }
}
