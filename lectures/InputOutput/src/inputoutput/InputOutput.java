/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inputoutput;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author mboyle
 */
public class InputOutput {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
//        readWriteBytesFile("read-sample.txt", "out-sample.txt");
//        readWriteCharsFile("read-sample.txt", "out-sample.txt");
//        readWriteCharsFileBuffered("read-sample.txt", "out-sample.txt");
//        runtimeComparisonDemo();
        scannerExample();
    }

    public static void readWriteBytesFile(String inputFileName
            , String outputFileName) throws IOException {
        FileInputStream in;
        FileOutputStream out;

        in = new FileInputStream(inputFileName);
        out = new FileOutputStream(outputFileName);
        int c;

        while ((c = in.read()) != -1) {
            out.write(c);
        }
    }

    public static void readWriteCharsFile(String inputFileName
            , String outputFileName) throws IOException {
        FileReader inputf;
        FileWriter outputf;
        int c;

        inputf = new FileReader(inputFileName);
        outputf = new FileWriter(outputFileName);

        while ((c = inputf.read()) != -1) {
            outputf.write(c);
        }
        outputf.close();
    }

    public static void readWriteCharsFileBuffered(String inputFileName
            , String outputFileName) throws IOException {
        BufferedReader inputfb = null;
        BufferedWriter outputfb = null;
        String readFromFile = "";
        inputfb = new BufferedReader(new FileReader(inputFileName));
        outputfb = new BufferedWriter(new FileWriter(outputFileName, true));

        while ((readFromFile = inputfb.readLine()) != null) {
            System.out.println(readFromFile);
            outputfb.write(readFromFile + "\n");
        }
    }
    
    public static void scannerExample(){
            System.out.println("Enter an integer to print to the console:");
        Scanner console = new Scanner(System.in);
        int number = console.nextInt();
        System.out.print("You entered: " + number + "\n");
    }

    public static void runtimeComparisonDemo() throws IOException {
        System.out.println("About to compare runtime of byte reader/writer vs."
                + "char reader/writer.");
        long start = System.currentTimeMillis();
        //reading/writing bytes (slower)
        readWriteBytesFile("read-sample_big-file.txt"
                , "out-sample_big-file.txt");
        long end = System.currentTimeMillis();

        System.out.println("Time taken for execution of byte reader/writer is "
                + (end - start));

        //reading/writing bytes (faster)
        start = System.currentTimeMillis();
        readWriteCharsFile("read-sample_big-file.txt"
                , "out-sample_big-file.txt");
        end = System.currentTimeMillis();
        System.out.println("Time taken for execution of char reader/writer is "
                + (end - start));
    }
}
