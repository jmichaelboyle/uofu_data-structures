/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package collectionsexample;

import java.util.Comparator;

/**
 *
 * @author mboyle
 */
public class StudentComparator implements Comparator {
    @Override
    public int compare(Object obj1, Object obj2){
        Student s1 = (Student) obj1;
        Student s2 = (Student) obj2;
        int result = 0;
        if (s1.GPA > s2.GPA){
            result = 1;
        } else if (s1.GPA < s2.GPA) {
            result = -1;
        }
        return result;
    }
}
