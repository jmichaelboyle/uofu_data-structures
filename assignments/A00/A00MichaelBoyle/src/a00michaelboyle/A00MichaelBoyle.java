/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package a00michaelboyle;

/**
 *
 * @author mboyle
 */
import java.math.BigDecimal;
import java.math.RoundingMode;

public class A00MichaelBoyle {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        //task 1 (see class definition)

        //task 2
        double brobovskySergie = 2.53;
        double careyPrice = 2.3;
        double anttiNiemi = 2.39;
        double jaroslavHalak = 2.29;
        double hardingJosh = 1.65;

        //e.g. initializing array to meaningful "null" values
        //double goalieArr[] = {-1.0, -1.0, -1.0, -1.0, -1.0};
        double goalieArr[] = new double[5]; //this initialzes all values to 0.0
        goalieArr[0] = brobovskySergie;
        goalieArr[1] = careyPrice;
        goalieArr[2] = anttiNiemi;
        goalieArr[3] = jaroslavHalak;
        goalieArr[4] = hardingJosh;

        //manage a separate array with the names of the goaltenders
        String goalieNameArr[] = new String[5];
        goalieNameArr[0] = "Brobovsky, Sergie";
        goalieNameArr[1] = "Price, Carey";
        goalieNameArr[2] = "Niemi, Antti";
        goalieNameArr[3] = "Halak, Jaroslav";
        goalieNameArr[4] = "Harding, Josh";

        //task 3
        for (int i = 0; i < goalieArr.length; i += 1) {
            if (goalieArr[i] >= 0.0 && goalieArr[i] < 2.0) {
                System.out.println(goalieNameArr[i] + ": Excellent Goaltending");
            } else if (goalieArr[i] >= 2.0 && goalieArr[i] < 2.5) {
                System.out.println(goalieNameArr[i] + ": Strong Goaltending");
            } else if (goalieArr[i] >= 2.5 && goalieArr[i] < 3.0) {
                System.out.println(goalieNameArr[i] + ": Fair Goaltending");
            } else if (goalieArr[i] >= 3.0){
                System.out.println(goalieNameArr[i] + ": Poor Goaltending");
            } else {
                //note that we'll throw an exception in this case
                //once we've covered exceptions in class
                System.out.println(goalieNameArr[i] + ": Invalid GAA provided");
            }
        }

        //task 4 (floating-point based)
        double fractionExcellent = 1.0/10.0;
        double fractionStrong = 1.0/2.0;
        double fractionFair = 1.0/5.0;
        double numberPoor = 10.0;
        
        double fractionPoor = 1.0 - (fractionExcellent + fractionStrong +
                fractionFair);
        
        double numberNonPoor = numberPoor * (fractionExcellent +
                fractionStrong + fractionFair) / fractionPoor;
        //System.out.println(numberNonPoor);
        
        double numberAll = numberNonPoor + numberPoor;
        //System.out.println(numberAll);
       
        double numberFair = fractionFair * numberAll;
        System.out.println("the number of fair is: " + numberFair);
//        // Task 4 (BigDecimal)
//        BigDecimal fractionExcellent = new BigDecimal(1.0 / 10.0);
//        BigDecimal fractionStrong = new BigDecimal(1.0 / 2.0);
//        BigDecimal fractionFair = new BigDecimal(1.0 / 5.0);
//        BigDecimal numberPoor = new BigDecimal(10.0);
//
//        BigDecimal fractionPoor = new BigDecimal(1.0).subtract(
//                fractionExcellent.add(fractionStrong).add(fractionFair));
//
//        BigDecimal numberNonPoor = 
//                (fractionExcellent.add(fractionStrong).add(
//                        fractionFair)).divide(
//                                fractionPoor, 2, RoundingMode.HALF_UP).multiply(
//                                        numberPoor);
//       
//        BigDecimal numberAll = numberPoor.add(numberNonPoor);
//        BigDecimal numberFair = numberAll.multiply(fractionFair);
//
//        System.out.println(numberFair);

        // Task 5 
        int salaryCap = 64300000;
        int averageSalary = 2400000;
        int currCapUsed = 0;
        int playerCount = 0;

        while (currCapUsed < salaryCap) {
            System.out.println("attempting to process player number " + (playerCount + 1));
            System.out.println("cap used at the start of the loop: " + currCapUsed);
            System.out.println("remaining cap space at the start of the loop: "
                    + (salaryCap - currCapUsed));
            if (salaryCap - currCapUsed >= averageSalary) {
                playerCount++;
                currCapUsed = playerCount * averageSalary;
            } else {
                //there may be some cap space left, however
                //it may be less than the average salary and
                //you cannot add a portion of a player to the roster
                System.out.println("unable to add player number " + (playerCount + 1));
                break;
            }
            System.out.println("cap used at the end of the loop: " + currCapUsed);
            System.out.println("remaining cap space at the end of the loop: "
                    + (salaryCap - currCapUsed));
        }
        System.out.println("the final number of players is: " + playerCount);
    }
}