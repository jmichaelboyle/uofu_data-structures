/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javabasicscontrolflow;

/**
 *
 * @author mboyle
 */
public class JavaBasicsControlFlow {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        controlFlowExample();
        loopExample();
        branchingExample();
    }

    public static void controlFlowExample() {
        int a = 10;
        int b = 5;

        // IF - ELSE example
        if (a >= b) {
            System.out.println("a is greater than b");
        } else {
            System.out.println("b is greater than a");
        }
    //Run the above program by changing the value of a and b

        // IF - ELSE IF example
        if (a > b) {
            System.out.println("a is greater than b");
        } else if (a == b) {
            System.out.println("a is equal to b");
        } else {
            System.out.println("b is greater than a");
        }

        // USING ?: OPERATOR
        int c = (a > b) ? a : b;
        System.out.println("The greatest among " + a + " and " + b + " is " + c); // check what happens if a and b are equal

    }

    public static void loopExample() {
        int count = 0;

        // WHILE LOOP
        while (count < 10) {
            System.out.println("In while loop, Count is " + count);
            count++;
        }
        // DO WHILE LOOP
        do {
            System.out.println("In do-while loop, Count is " + count);
            count++;
        } while (count < 20); // Change the count value and condition value to 10 and check which loop executes atleast once

        // FOR LOOP
        for (int i = 1; i < 10; i++) {
            // code here
            System.out.println("In For loop, the value of i = " + i);
        }

    }

    public static void branchingExample() {
        int count;
        for (count = 0; count < 20; count++) {
            if (count > 5 && count <= 10) {
                System.out.println("Inside IF flow,Count = " + count);
                continue;
            } else if (count > 10) {
                System.out.println("Inside ELSE IF flow,Count i= " + count);
                break;
            }
            System.out.println("Inside For loop, Count is " + count);
        }
    }
}