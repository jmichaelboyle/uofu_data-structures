/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ioerrorsandexceptions;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author mboyle
 */
public class IOErrorsAndExceptions {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException{
       readWriteCharsFileBufferedWithTryCatch();
       readWriteCharsFileBufferedExcept();
       readWriteCharsFileBufferedWithTryCatchJava7Way();
       
    }

    public static void readWriteCharsFileBufferedWithTryCatch() throws IOException {
        BufferedReader inputfb = null;
        BufferedWriter outputfb = null;
        try {
            inputfb = new BufferedReader(new FileReader("read-sample.txt"));
            outputfb = new BufferedWriter(new FileWriter("out-sample.txt", true));
                    
            String readFromFile;
            while ((readFromFile = inputfb.readLine()) != null) {
                System.out.println(readFromFile);
                outputfb.write(readFromFile + "\n");
            }

        } catch (IOException ie) {
            System.out.println(ie);
        } finally {
            if (inputfb != null) {
                inputfb.close();
            }
            if (outputfb != null) {
                outputfb.close();
            }
        }
    }

    public static void readWriteCharsFileBufferedExcept() throws IOException {
        BufferedReader inputfb = null;
        BufferedWriter outputfb = null;
        try {
            inputfb = new BufferedReader(new FileReader("a-file-that-does-not-exist.txt"));
            outputfb = new BufferedWriter(new FileWriter("outsample.txt", true));
            String readFromFile;    
            while ((readFromFile = inputfb.readLine()) != null) {
                System.out.println(readFromFile);
                outputfb.write(readFromFile + "\n");
            }

        } catch (IOException ie) {
            System.out.println(ie);
        } finally {
            if (inputfb != null) {
                inputfb.close();
            }
            if (outputfb != null) {
                outputfb.close();
            }
        }
    }
    public static void readWriteCharsFileBufferedWithTryCatchJava7Way() throws IOException {
        //this try with resource approach is avaialble starting in Java7
        try (BufferedReader inputfb = new BufferedReader(new FileReader(
                "read-sample.txt"));
             BufferedWriter outputfb = new BufferedWriter(new FileWriter(
                "out-sample.txt", true));   
                ){
            String readFromFile;
            while ((readFromFile = inputfb.readLine()) != null) {
                System.out.println(readFromFile);
                outputfb.write(readFromFile + "\n");
            }
        } catch (IOException ie) {
            System.out.println(ie);
        } 
    }
}
