/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objectorientedconcepts;

/**
 *
 * @author mboyle
 */
public class ObjectOrientedConcepts {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        ScientificCalculator myCalculator = new ScientificCalculator();
        myCalculator.operand1 = 4.7f;
        myCalculator.operand2 = 3.1f;
        
        ScientificCalculator myCalculator2 = new ScientificCalculator();
        myCalculator2.operand1 = 5.7f;
        myCalculator2.operand2 = 5.1f;
        
        float result = myCalculator.add();
        System.out.println("result of add is: " + result);
        float result2 = myCalculator.xSquared();
        System.out.println("result of xSquared is: " + result2);
        
        System.out.println("result of add on second calc is:" + myCalculator2.add());
        
        Calculator mySimpleCalc = new Calculator();
        mySimpleCalc.operand1 = 3.4f;
        mySimpleCalc.operand2 = 7.4f;
        System.out.println("add from simple calc is:" + mySimpleCalc.add());
        
        
        
        
    }
    
}
