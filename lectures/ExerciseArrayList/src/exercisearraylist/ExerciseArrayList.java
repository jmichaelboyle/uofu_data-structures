/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package exercisearraylist;

import java.util.ArrayList;

/**
 *
 * @author JMB
 */
public class ExerciseArrayList {

  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) {
    ArrayList<Integer> myList = new ArrayList<>();
    //TODO(student): note that we didn't have to declare the length
    myList.add(5);
    myList.add(7);
    myList.add(9);
    myList.remove(0);
    myList.remove(0);
    //TODO(student): observe the instance as we add and remove
    
    //TODO(student): compare to an array
    int[] myArr = new int[5];
    myArr[0] = 5;
    myArr[1] = 7;
    myArr[2] = 9;
    myArr[0] = 0;
    myArr[1] = 0;
    myArr[2] = 0;
    
    //TODO(student): create an ArrayList of players (i.e. Player class instances)
    ArrayList<Player> myListPlayer = new ArrayList<>();
    Player myPlayer1 = new Player("Duncan Keith");
    myListPlayer.add(myPlayer1);
    Player myPlayer2 = new Player("Patrick Kane");
    myListPlayer.add(myPlayer2);
    
    //TODO(student): create an array of players (i.e. Player class instances)
    Player[] myArrPlayer = new Player[2];
    Player myPlayer3 = new Player("Alex Ovechkin");
    Player myPlayer4 = new Player("Braden Holtby");
    myArrPlayer[0] = myPlayer3;
    myArrPlayer[1] = myPlayer4;
    
  }  
}
