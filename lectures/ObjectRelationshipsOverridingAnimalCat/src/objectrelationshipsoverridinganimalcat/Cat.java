/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package objectrelationshipsoverridinganimalcat;

/**
 *
 * @author mboyle
 */
public class Cat extends Animal{
    public static void testClassMethod(){
        System.out.println("In class method of Cat.");
    }
    
    @Override
    public void testInstanceMethod(){
        System.out.println("In instance method of Cat.");
    }
    
}
