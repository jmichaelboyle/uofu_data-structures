/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package a2michaelboyleworking1;

import java.net.URL;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.io.IOException;

import java.util.Scanner;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.FileReader;

/**
 *
 * @author JMB
 */
public class A2MichaelBoyleWorking1 {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {

        System.out.println("Welcome to the NHL play by play event scraper. The "
                + "system will get events for the specified game for the "
                + "specified event type.");

        Scanner console = new Scanner(System.in);
        String userInput;

        do {
            System.out.println("enter the letter A:");
            userInput = console.nextLine();
            if (userInput.equals("A")) {
                System.out.println("correct input");
                do {
                    System.out.println("enter the letter B:");
                    userInput = console.nextLine();
                    if (userInput.contains("B")) {
                        System.out.println("correct input");
                    } else {
                        if (!userInput.equals("EXIT")) {
                            System.out.println("wrong input");
                        }
                        continue;
                    }
                    break;
                } while (!userInput.equals("EXIT"));
            } else {
                if (!userInput.equals("EXIT")) {
                    System.out.println("wrong input");
                }
            }
        } while (!userInput.equals("EXIT"));

        //example writing to file without handling exceptions
        BufferedWriter writer;
        writer = new BufferedWriter(new FileWriter(
                "some_file.csv", true));
        writer.write("A,B,C,C\n");
        writer.close();

        //example writing to file handling exceptions
        BufferedWriter writer2 = null;
        try {
            writer2 = new BufferedWriter(new FileWriter("some_file2.csv", true));
            writer2.write("A,B,C,C\n");
        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        } finally {
            if (writer2 != null) {
                writer2.close();
            }
        }
        //output Bye at end of program
        System.out.println("Bye");
    }

    /**
     *
     * Writes the given record to the correct event data file
     *
     * @param gameNumber
     * @param eventType
     * @param record
     * @return
     * @throws IOException
     */
    public static int writeRecordToFile(int gameNumber, String eventType, String record) throws IOException {
        int success = -1;

        //TODO: your logic here
        return success;
    }

    /**
     *
     * Reads and outputs data from the given event data file
     *
     * @param gameNumber
     * @param eventType
     * @return
     * @throws IOException
     */
    public static int printRecordsFromFile(int gameNumber, String eventType) throws IOException {
	// write code to read line by line from file and print to screen.
        // also create a success variable to indicate the status of the 
        // file IO operation

        int success = -1;

        //TODO: your logic here
        return success;
    }

    /**
     *
     * Helper method to create an event data file name from a game number and an
     * event type
     *
     * IMPORTANT: there is no need to change (or even use) this method for the
     * assignment
     *
     * @param gameNumber
     * @param eventType
     * @return
     */
    static String createDataFileName(int gameNumber, String eventType) {
        return gameNumber + "_" + eventType.toUpperCase() + ".csv";
    }

    /**
     * Gets the nth event of the specified type from the specified game number.
     *
     * IMPORTANT: there is no need to change this method for the assignment
     *
     * @param n
     * @param eventType
     * @param gameNumber
     * @return td HTML tag with event data
     */
    public static String getNthEventByType(int n, String eventType, int gameNumber) {

        String urlText = "http://www.nhl.com/scores/htmlreports/20132014/PL0"
                + gameNumber + ".HTM";
        URL url;
        InputStream is = null;
        BufferedReader br;
        String line = "not found"; //initialize result line to "not found"
        int eventCount = 0;

        try {
            url = new URL(urlText);
            is = url.openStream();  // throws an IOException
            br = new BufferedReader(new InputStreamReader(is));

            while (eventCount < n && (line = br.readLine()) != null) {
                if (line.contains(eventType.toUpperCase())) {
                    //we know the next line is the face-off details
                    line = br.readLine();
                    eventCount += 1;
                }
            }
        } catch (MalformedURLException mue) {
            System.out.println(mue.getMessage());
        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        } finally {
            try {
                if (is != null) {
                    is.close();
                }
            } catch (IOException ioe) {
                // nothing to see here
            }
        }
        return line;
    }

    /**
     * Gets shot data from the given <td> tag which results from the method
     * getNthEventByType
     *
     * IMPORTANT: there is no need to change this method for the assignment
     *
     * @param eventHTML
     * @return
     */
    public static String getShotDataFromEventHTML(String eventHTML) {
        String result;

        //get shooter team
        int positionOfFirstGreaterThan = eventHTML.indexOf(">", 0);
        String shooterTeam = eventHTML.substring(positionOfFirstGreaterThan + 1, positionOfFirstGreaterThan + 4);

        //get shooter number
        int positionOfFirstHash = eventHTML.indexOf("#", 0);
        int positionOfFirstSpaceAfterFirstHash = eventHTML.indexOf(" ", positionOfFirstHash);
        String shooterPlayerNumber = eventHTML.substring(positionOfFirstHash + 1,
                positionOfFirstSpaceAfterFirstHash);

        //get shooter name
        int positionOfFirstCommaSpaceAfterFirstHash = eventHTML.indexOf(
                ", ", positionOfFirstSpaceAfterFirstHash + 1);
        String shooterLastName = eventHTML.substring(
                positionOfFirstSpaceAfterFirstHash + 1, positionOfFirstCommaSpaceAfterFirstHash);

        //get shot type
        int positionOfSecondCommaSpaceAfterShooterName = eventHTML.indexOf(
                ", ", positionOfFirstCommaSpaceAfterFirstHash + 1);
        String shotType = eventHTML.substring(positionOfFirstCommaSpaceAfterFirstHash + 2, positionOfSecondCommaSpaceAfterShooterName);

        //get zone
        int positionOfThirdCommaSpaceAfterShooterName = eventHTML.indexOf(
                ", ", positionOfSecondCommaSpaceAfterShooterName + 1);
        String zone = eventHTML.substring(positionOfSecondCommaSpaceAfterShooterName + 2, positionOfThirdCommaSpaceAfterShooterName);

        //get length
        int positionOfFirstSpaceAfterThirdCommaSpaceAfterShooterName = eventHTML.indexOf(
                " ", positionOfThirdCommaSpaceAfterShooterName + 2);
        String length = eventHTML.substring(positionOfThirdCommaSpaceAfterShooterName + 2, positionOfFirstSpaceAfterThirdCommaSpaceAfterShooterName);

        result = shooterTeam + "," + shooterPlayerNumber + "," + shooterLastName
                + "," + shotType + "," + zone + "," + length;
        return result;
    }
}
